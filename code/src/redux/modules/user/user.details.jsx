//Services
export const SHOW_DETAILS = "modules/users/SHOW_DETAILS";



const initialState = {
    show: false
};

export default (state=initialState, action) => {
    switch (action.type) {
        case SHOW_DETAILS:
            return {
                ...state,
                id: action.id,
                show: action.show,
            };

        default:
            return state;
    }
};

export const showUserDetails = (show, id) => {
    return (dispatch, getState) => {    
        dispatch({
            type: SHOW_DETAILS,
            show: show,
            id: id
        })
    }
}

