//Services
import _ from "lodash";
import api from "config/api";
import request from "services/request";

export const FETCH_REQUEST = "modules/users/FETCH_REQUEST";
export const FETCH_SUCCESS = "modules/users/FETCH_SUCCESS";
export const FETCH_ERROR = "modules/users/FETCH_ERROR";
export const UPDATE_DATA = "modules/users/UPDATE_DATA";

const options = [FETCH_REQUEST, FETCH_SUCCESS, FETCH_ERROR];

const initialState = {
    isFetching: false
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_REQUEST:
            return {
                ...state,
                isFetching: true
            };

        case FETCH_ERROR:
            return {
                ...state,
                error: action.error,
                isFetching: false
            };

        case FETCH_SUCCESS:
            return {
                ...state,
                data: action.data.list,
                isFetching: false
            };

        case UPDATE_DATA:
            return {
                ...state,
                data: action.data
            };

        default:
            return state;
    }
};

export const getUsers = () => {
    let req = api["users.get"];

    return (dispatch, getState) => {
        if(!getState().usersGet.data){
            request(req, options, dispatch, getState);
        }
    };
};

export const sortUsers = (sortBy) => {
    return (dispatch, getState) => {
        let list = getState().usersGet.data;
        let newList = _.sortBy(list, sortBy);
        dispatch({
            type: UPDATE_DATA,
            data: newList
        })
    }
}
