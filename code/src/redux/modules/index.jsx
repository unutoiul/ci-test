import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";

import usersGet from 'redux/modules/user/users.get';
import userDetails from 'redux/modules/user/user.details';

export default combineReducers({
    router: routerReducer,
    userDetails,
    usersGet
});
