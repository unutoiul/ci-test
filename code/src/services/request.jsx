// Services
import fetch from "services/fetch";

const request = (req, options, dispatch, getState) => {
    dispatch({
        type: options[0] // request fetch
    });

    return fetch(req.url, req.type, req.data)
        .then(res =>
            dispatch({
                type: options[1],
                successful: req.successful,
                data: res.body
            })
        )
        .catch(error =>
            dispatch({
                type: options[2],
                error: req.error
            })
        );
};

export default request;
