//React
import React from "react";
import { render } from "react-dom";

// Redux
import { Provider } from "react-redux";
import store from "redux/store/store";

import 'index.css';
import App from 'App';
import registerServiceWorker from 'registerServiceWorker';

const target = document.querySelector("#root");

render(
    <Provider store={store}>
        <App />
    </Provider>,
    target
);

registerServiceWorker();
