import React, { Component } from 'react';

import UserCard from 'components/user/user.card';
import UserCardDetails from 'components/user/user.details';


// Redux
import { connect } from "react-redux";
import { getUsers, sortUsers } from "redux/modules/user/users.get";
import { showUserDetails } from "redux/modules/user/user.details";

const mapStateToProps = (state, props) => {
    return {
        users: {
            isFetching: state.usersGet.isFetching,
            data: state.usersGet.data,
            error: state.usersGet.error
        },
        userDetails: {
            id: state.userDetails.id,
            show: state.userDetails.show
        }
    };
};

const mapDispatchToProps = {
    showUserDetails,
    getUsers,
    
    sortUsers
};

class Main extends Component {
    constructor(props) {
        super(props);

        this.sortBy = this.sortBy.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    closeModal(event){
        this.props.showUserDetails(false);
    }

    sortBy(event){
        let sortBy = event.target.value;
        this.props.sortUsers(sortBy);
    }

    componentDidMount() {
        this.props.getUsers();
    }

    render() {
        const users = this.props.users;
        
        return (
            <div>
                {users.isFetching && (
                    <span>{users.loading}</span>
                )}
        
                {users.error && (
                    <span>{users.error}</span>
                )}
                    <div className="header">
                        <h3>Our Employees</h3>
                        <div className="filter">
                            Sort by: 
                            <select  onChange={this.sortBy}>
                                <option value="firstName">First Name</option>
                                <option value="lastName">Last Name</option>
                                <option value="jobTitle">Job Title</option>
                                <option value="dateJoined">Date Joined</option>
                                <option value="age">Age</option>
                            </select>
                        </div>
                    </div>
                
                {
                    this.state && this.state.sortBy && this.state.sortBy
                }
                

                <div className="container">
                {
                    users.data && users.data.map((data, key)=>{
                        return(
                            <UserCard id={key} key={key} data={data} />
                        );
                    })
                }

                </div>

                {
                    this.props.userDetails.show &&
                    <div className="modal" onClick={this.closeModal}>
                        <UserCardDetails data={users.data[this.props.userDetails.id]}/>
                    </div>
                }
             </div>
        );
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(
    Main
);
