const api = {
    "users.get": {
        url: "/data/data.json",
        type: "get",
        successful: "Users loaded successfully.",
        error:
            "Unable to load users data.",
        data: null
    },
}
export default api;
