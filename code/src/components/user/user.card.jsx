import React, { Component } from 'react';

// Redux
import { connect } from "react-redux";
import { showUserDetails } from "redux/modules/user/user.details";


const mapDispatchToProps = {
  showUserDetails
};

class UserCard extends Component {
  constructor(props) {
    super(props);
    this.openDetails = this.openDetails.bind(this);
  }

  openDetails(event){
    this.props.showUserDetails(true, this.props.id);
  }

  render() {
    return (
      <div className="user-card" onClick={this.openDetails}>
        <div className="user-wrapper">
          <div className="user-info">
            <div className="user-picture">
          </div>
          </div>
          <div className="user-desc">
            <b>{this.props.data.firstName} {this.props.data.lastName}</b>
            <p>{this.props.data.description}</p>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(null, mapDispatchToProps)(
  UserCard
);
