// Redux
    import React, { Component } from 'react';

class UserCardDetails extends Component {
    render() {
        return (
        <div className="user-card-details" >
            <div className="user-wrapper">
                <div className="user-info">
                    <div className="user-picture"></div>
                    <p>{this.props.data.age}</p>
                    <p>{this.props.data.jobTitle}</p>
                    <p>{this.props.data.dateJoined}</p>
                </div>
                <div className="user-desc">
                    <h3>{this.props.data.firstName} {this.props.data.lastName}</h3>
                    <p>{this.props.data.description}</p>
                </div>
            </div>
        </div>
        );
    }
}

export default UserCardDetails;

